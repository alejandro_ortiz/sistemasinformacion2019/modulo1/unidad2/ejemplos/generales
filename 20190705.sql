﻿USE ciclistas;
/**
  cuantas etapas ha ganado cada ciclista (dorsal,etapas)
*/

  SELECT 
    e.dorsal, 
    COUNT(*)numeroEtapas 
  FROM 
    etapa e 
  GROUP BY 
    e.dorsal; 

/**
  nombre de los ciclistas que han ganado mas de 2 etapas
*/

  -- (ciclistas que han ganado mas de 2 etapas) c1
  SELECT 
     e.dorsal
  FROM 
    etapa e 
  GROUP BY 
    e.dorsal
  HAVING 
    COUNT(*)>2;

  -- completa
  SELECT 
    c.nombre 
  FROM 
    ciclista c 
  JOIN (
    SELECT 
      e.dorsal
    FROM 
      etapa e 
    GROUP BY 
      e.dorsal
    HAVING 
      COUNT(*)>2) c1 
  USING 
    (dorsal);

/**
  nombre del ciclista que tiene mas edad
*/

-- maxima edad
  SELECT MAX(c.edad)edadMaxima FROM ciclista c; 

-- completa
  SELECT 
    c.nombre 
  FROM (
    SELECT 
      MAX(c.edad) edadMaxima
    FROM 
      ciclista c) c1
  JOIN 
    ciclista c 
  ON 
    c.edad=c1.edadMaxima; 