﻿USE paginaweb;

/*
  1. Titulo del libro y nombre del autor que lo ha escrito
*/

  -- Con Join
  SELECT l.titulo, a.nombre_completo 
    FROM libro l JOIN autor a 
    ON l.autor = a.id_autor;

  -- Sin Join (producto cartesiano)
  SELECT l.titulo, a.nombre_completo
    FROM libro l, autor a
    WHERE l.autor = a.id_autor;

  -- Con Join como producto cartesiano (no utilizar)
  SELECT l.titulo, a.nombre_completo 
    FROM libro l JOIN autor a 
    WHERE l.autor = a.id_autor;

/*
  2. Nombre del autor y titulo del libro en el que han ayudado
*/

  SELECT a1.nombre_completo, l.titulo 
    FROM libro l 
    JOIN ayuda a ON l.id_libro = a.libro 
    JOIN autor a1 ON a.autor = a1.id_autor;

/*
  3. Los libros que se ha descargado cada usuario con la fecha de descarga. 
  Listar el login, fecha de descarga, id_libro
*/
  
  SELECT f.libro,
         f.usuario,
         f.fecha_descarga
    FROM fechadescarga f;

/*
  4. Los libros que se ha descargado cada usuario con la fecha de descarga. 
  Listar el login, correo,  fecha de descarga, titulo del libro
*/

  SELECT u.login, u.email, f.fecha_descarga, l.titulo
    FROM usuario u 
    JOIN descarga d ON u.login = d.usuario
    JOIN fechadescarga f USING (usuario, libro)
    JOIN libro l ON d.libro = l.id_libro;

  SELECT u.login, u.email, f.fecha_descarga, l.titulo
    FROM usuario u 
    JOIN descarga d 
    JOIN fechadescarga f 
    JOIN libro l
    ON d.libro = l.id_libro 
    AND u.login = d.usuario 
    AND d.usuario = f.usuario 
    AND d.libro = f.libro;

/*
  5. El numero de libros que hay
*/

  SELECT COUNT(*)numLibros
    FROM libro l;

/*
  6. El numero de libros por coleccion (no hace falta colocar nombre de la coleccion)
*/

  SELECT l.coleccion, COUNT(*)numLibros 
    FROM libro l 
    GROUP BY l.coleccion;

/*
  7. La coleccion que tiene mas libros (no hace falta colocar nombre de la coleccion)
*/

  -- Subconsulta C1
  SELECT l.coleccion, COUNT(*)numLibros 
    FROM libro l 
    GROUP BY l.coleccion;

  -- Subconsulta C2
  SELECT MAX(c1.numLibros)maximo 
    FROM (
      SELECT l.coleccion, COUNT(*)numLibros 
        FROM libro l 
        GROUP BY l.coleccion
    ) c1;

  /************
    CON JOIN
  ************/
  -- Consulta final
  SELECT c1.coleccion
    FROM (
      SELECT l.coleccion, COUNT(*)numLibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1
    JOIN (
      SELECT MAX(c1.numLibros)maximo 
        FROM (
        SELECT l.coleccion, COUNT(*)numLibros 
          FROM libro l 
          GROUP BY l.coleccion
        ) c1
      ) c2
    ON c1.numLibros = c2.maximo;

  /*************
    CON WHERE
  *************/
  -- Consulta final
  SELECT c1.coleccion
    FROM (
      SELECT l.coleccion, COUNT(*)numLibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1
    WHERE c1.numLibros = (
      SELECT MAX(c1.numLibros)maximo 
        FROM (
          SELECT l.coleccion, COUNT(*)numLibros 
            FROM libro l 
            GROUP BY l.coleccion
        ) c1
      );

  /*************
    CON HAVING
  *************/
  -- Consulta final
  SELECT l.coleccion, COUNT(*)numLibros 
    FROM libro l 
    GROUP BY l.coleccion
    HAVING numLibros = (
      SELECT MAX(c1.numLibros)maximo 
        FROM (
          SELECT l.coleccion, COUNT(*)numLibros 
            FROM libro l 
            GROUP BY l.coleccion
        ) c1
      );
/*
  8. La coleccion que tiene menos libros (no hace falta colocar nombre de la coleccion)
*/

  -- Subconsulta C1
  SELECT l.coleccion, COUNT(*)numLibros 
    FROM libro l 
    GROUP BY l.coleccion;

  -- Subconsulta C2
  SELECT MIN(c1.numLibros)minimo 
    FROM (
      SELECT l.coleccion, COUNT(*)numLibros 
        FROM libro l 
        GROUP BY l.coleccion
    ) c1;

  /************
    CON JOIN
  ************/
  -- Consulta final
  SELECT c1.coleccion
    FROM (
      SELECT l.coleccion, COUNT(*)numLibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1
    JOIN (
      SELECT MIN(c1.numLibros)minimo 
        FROM (
        SELECT l.coleccion, COUNT(*)numLibros 
          FROM libro l 
          GROUP BY l.coleccion
        ) c1
      ) c2
    ON c1.numLibros = c2.minimo;

  /*************
    CON WHERE
  *************/
  -- Consulta final
  SELECT c1.coleccion
    FROM (
      SELECT l.coleccion, COUNT(*)numLibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1
    WHERE c1.numLibros = (
      SELECT MIN(c1.numLibros)minimo 
        FROM (
          SELECT l.coleccion, COUNT(*)numLibros 
            FROM libro l 
            GROUP BY l.coleccion
        ) c1
      );

  /*************
    CON HAVING
  *************/
  -- Consulta final
  SELECT l.coleccion, COUNT(*)numLibros 
    FROM libro l 
    GROUP BY l.coleccion
    HAVING numLibros = (
      SELECT MIN(c1.numLibros)minimo 
        FROM (
          SELECT l.coleccion, COUNT(*)numLibros 
            FROM libro l 
            GROUP BY l.coleccion
        ) c1
      );

/*
  9. El nombre de la coleccion que tiene mas libros
*/

-- Subconsulta C1
  SELECT l.coleccion, COUNT(*)numLibros 
    FROM libro l 
    GROUP BY l.coleccion;

  -- Subconsulta C2
  SELECT MAX(c1.numLibros)maximo 
    FROM (
      SELECT l.coleccion, COUNT(*)numLibros 
        FROM libro l 
        GROUP BY l.coleccion
    ) c1;

  /************
    CON JOIN
  ************/
  -- Subconsulta C3
  SELECT c1.coleccion
    FROM (
      SELECT l.coleccion, COUNT(*)numLibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1
    JOIN (
      SELECT MAX(c1.numLibros)maximo 
        FROM (
        SELECT l.coleccion, COUNT(*)numLibros 
          FROM libro l 
          GROUP BY l.coleccion
        ) c1
      ) c2
    ON c1.numLibros = c2.maximo;

  -- Consulta final
  SELECT c.nombre 
    FROM coleccion c 
      JOIN (
        SELECT c1.coleccion
          FROM (
            SELECT l.coleccion, COUNT(*)numLibros 
              FROM libro l 
              GROUP BY l.coleccion
            ) c1
          JOIN (
            SELECT MAX(c1.numLibros)maximo 
              FROM (
              SELECT l.coleccion, COUNT(*)numLibros 
                FROM libro l 
                GROUP BY l.coleccion
              ) c1
            ) c2
          ON c1.numLibros = c2.maximo
      ) consulta7 
    ON consulta7.coleccion= c.id_coleccion;

  /*************
    CON WHERE
  *************/
  -- Subconsulta C3
  SELECT c1.coleccion
    FROM (
      SELECT l.coleccion, COUNT(*)numLibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1
    WHERE c1.numLibros = (
      SELECT MAX(c1.numLibros)maximo 
        FROM (
          SELECT l.coleccion, COUNT(*)numLibros 
            FROM libro l 
            GROUP BY l.coleccion
        ) c1
      );

  -- Consulta final
  
  SELECT c.nombre 
    FROM coleccion c 
      JOIN (
        SELECT c1.coleccion
          FROM (
            SELECT l.coleccion, COUNT(*)numLibros 
              FROM libro l 
              GROUP BY l.coleccion
            ) c1
          WHERE c1.numLibros = (
            SELECT MAX(c1.numLibros)maximo 
              FROM (
                SELECT l.coleccion, COUNT(*)numLibros 
                  FROM libro l 
                  GROUP BY l.coleccion
              ) c1
           )
        )consulta7 
      ON consulta7.coleccion = c.id_coleccion; 

  /*************
    CON HAVING
  *************/
  -- Subconsulta C3
  SELECT l.coleccion, COUNT(*)numLibros 
    FROM libro l 
    GROUP BY l.coleccion
    HAVING numLibros = (
      SELECT MAX(c1.numLibros)maximo 
        FROM (
          SELECT l.coleccion, COUNT(*)numLibros 
            FROM libro l 
            GROUP BY l.coleccion
        ) c1
      );

  -- Consulta final
  SELECT c.nombre 
    FROM coleccion c 
      JOIN (
        SELECT l.coleccion, COUNT(*)numLibros 
          FROM libro l 
          GROUP BY l.coleccion
          HAVING numLibros = (
            SELECT MAX(c1.numLibros)maximo 
              FROM (
                SELECT l.coleccion, COUNT(*)numLibros 
                  FROM libro l 
                  GROUP BY l.coleccion
              ) c1
           )
        )consulta7 
      ON c.id_coleccion= consulta7.coleccion;

/*
  10. El nombre de la coleccion que tiene menos libros
*/

  -- Subconsulta C1
  SELECT l.coleccion, COUNT(*)numLibros 
    FROM libro l 
    GROUP BY l.coleccion;

  -- Subconsulta C2
  SELECT MIN(c1.numLibros)minimo 
    FROM (
      SELECT l.coleccion, COUNT(*)numLibros 
        FROM libro l 
        GROUP BY l.coleccion
    ) c1;

  /************
    CON JOIN
  ************/
  -- Subconsulta C3
  SELECT c1.coleccion
    FROM (
      SELECT l.coleccion, COUNT(*)numLibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1
    JOIN (
      SELECT MIN(c1.numLibros)minimo 
        FROM (
        SELECT l.coleccion, COUNT(*)numLibros 
          FROM libro l 
          GROUP BY l.coleccion
        ) c1
      ) c2
    ON c1.numLibros = c2.minimo;

  -- Consulta final
  SELECT c.nombre 
    FROM coleccion c 
      JOIN (
        SELECT c1.coleccion
          FROM (
            SELECT l.coleccion, COUNT(*)numLibros 
              FROM libro l 
              GROUP BY l.coleccion
            ) c1
          JOIN (
            SELECT MIN(c1.numLibros)minimo 
              FROM (
              SELECT l.coleccion, COUNT(*)numLibros 
                FROM libro l 
                GROUP BY l.coleccion
              ) c1
            ) c2
          ON c1.numLibros = c2.minimo
       ) consulta8 
    ON c.id_coleccion = conculta8.coleccion;

  /*************
    CON WHERE
  *************/
  -- Subconsulta C3
  SELECT c1.coleccion
    FROM (
      SELECT l.coleccion, COUNT(*)numLibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1
    WHERE c1.numLibros = (
      SELECT MIN(c1.numLibros)minimo 
        FROM (
          SELECT l.coleccion, COUNT(*)numLibros 
            FROM libro l 
            GROUP BY l.coleccion
        ) c1
      );

  -- Consulta final
  SELECT c.nombre 
    FROM coleccion c 
      JOIN (
        SELECT c1.coleccion
          FROM (
            SELECT l.coleccion, COUNT(*)numLibros 
              FROM libro l 
              GROUP BY l.coleccion
            ) c1
          WHERE c1.numLibros = (
            SELECT MIN(c1.numLibros)minimo 
              FROM (
                SELECT l.coleccion, COUNT(*)numLibros 
                  FROM libro l 
                  GROUP BY l.coleccion
              ) c1
           )
        ) consulta8 
      ON c.id_coleccion = consulta8.coleccion;

  /*************
    CON HAVING
  *************/
  -- Subconsulta C3
  SELECT l.coleccion, COUNT(*)numLibros 
    FROM libro l 
    GROUP BY l.coleccion
    HAVING numLibros = (
      SELECT MIN(c1.numLibros)minimo 
        FROM (
          SELECT l.coleccion, COUNT(*)numLibros 
            FROM libro l 
            GROUP BY l.coleccion
        ) c1
      );

  -- Consulta final
  SELECT c.nombre 
    FROM coleccion c 
      JOIN (
        SELECT l.coleccion, COUNT(*)numLibros 
          FROM libro l 
          GROUP BY l.coleccion
          HAVING numLibros = (
            SELECT MIN(c1.numLibros)minimo 
              FROM (
                SELECT l.coleccion, COUNT(*)numLibros 
                  FROM libro l 
                  GROUP BY l.coleccion
              ) c1
            )
        ) consulta8 
      ON c.id_coleccion = consulta8.coleccion;

/*
  11. El nombre del libro que se ha descargado mas veces
*/

  -- Subconsulta C1
  SELECT f.libro, COUNT(*)numDescargas 
    FROM fechadescarga f 
    GROUP BY f.libro;

  -- Subconsulta C2
  SELECT MAX(c1.numDescargas)maximo 
    FROM (
      SELECT f.libro, COUNT(*)numDescargas 
        FROM fechadescarga f 
        GROUP BY f.libro) c1;

  -- Subconsulta C3
  SELECT c1.libro 
    FROM (
      SELECT f.libro, COUNT(*)numDescargas 
        FROM fechadescarga f 
        GROUP BY f.libro
      ) c1
    JOIN (
      SELECT MAX(c1.numDescargas)maximo 
        FROM (
          SELECT f.libro, COUNT(*)numDescargas 
            FROM fechadescarga f 
            GROUP BY f.libro
        ) c1
      ) c2 
    ON c1.numDescargas = c2.maximo;
    
  -- Consulta final
  SELECT l.titulo 
    FROM libro l 
      JOIN (
        SELECT c1.libro 
          FROM (
            SELECT f.libro, COUNT(*)numDescargas 
              FROM fechadescarga f 
              GROUP BY f.libro
            ) c1
          JOIN (
            SELECT MAX(c1.numDescargas)maximo 
              FROM (
                SELECT f.libro, COUNT(*)numDescargas 
                  FROM fechadescarga f 
                  GROUP BY f.libro
              ) c1
            ) c2 
          ON c1.numDescargas = c2.maximo
        ) c3 
      ON l.id_libro = c3.libro;
/*
  12. El nombre del usuario que ha descargado más libros
*/

  -- Subconsulta C1
  SELECT f.usuario, COUNT(*)numDescargas 
    FROM fechadescarga f 
    GROUP BY f.usuario;

  -- Subconsulta C2
  SELECT MAX(c1.numDescargas)maximo 
    FROM (
      SELECT f.usuario, COUNT(*)numDescargas 
        FROM fechadescarga f 
        GROUP BY f.usuario) c1;

  -- Consulta final
  SELECT c1.usuario 
    FROM (
      SELECT f.usuario, COUNT(*)numDescargas 
        FROM fechadescarga f 
        GROUP BY f.usuario
      ) c1 
    JOIN (
      SELECT MAX(c1.numDescargas)maximo 
        FROM (
          SELECT f.usuario, COUNT(*)numDescargas 
            FROM fechadescarga f 
            GROUP BY f.usuario
        ) c1
      ) c2 
    ON c1.numDescargas = c2.maximo;

/*
  13. El nombre de los usuarios que han descargado mas libros que Adam3
*/

  -- Subconsulta C1
  SELECT f.usuario, COUNT(*)numDescargas 
    FROM fechadescarga f 
    GROUP BY f.usuario;

  -- Subconsulta C2
  SELECT c1.numDescargas 
    FROM (
      SELECT f.usuario, COUNT(*)numDescargas 
        FROM fechadescarga f 
        GROUP BY f.usuario
      ) c1 
    WHERE c1.usuario = 'Adam3';

  /************
    CON JOIN
  ************/
  -- Consulta final

  SELECT c1.usuario 
    FROM (
      SELECT f.usuario, COUNT(*)numDescargas 
        FROM fechadescarga f 
        GROUP BY f.usuario
      ) c1 
    JOIN(
      SELECT c1.numDescargas 
        FROM (
          SELECT f.usuario, COUNT(*)numDescargas 
            FROM fechadescarga f 
            GROUP BY f.usuario
          ) c1 
        WHERE c1.usuario = 'Adam3'
      )c2 
    ON c1.numDescargas > c2.numDescargas;

  /*************
    CON WHERE
  *************/
  -- Consulta final
  SELECT c1.usuario 
    FROM (
      SELECT f.usuario, COUNT(*)numDescargas 
        FROM fechadescarga f 
        GROUP BY f.usuario
      ) c1 
    WHERE c1.numDescargas > (
      SELECT c1.numDescargas 
        FROM (
          SELECT f.usuario, COUNT(*)numDescargas 
            FROM fechadescarga f 
            GROUP BY f.usuario
          ) c1 
        WHERE c1.usuario = 'Adam3');

/*
  14. El mes que mas libros se han descargado 
*/

  -- Subconsulta C1 
  SELECT MONTH(f.fecha_descarga)mes, COUNT(*)descargasPorMes 
    FROM fechadescarga f 
    GROUP BY MONTH(f.fecha_descarga);

  -- Subconsulta C2
  SELECT MAX(c1.descargasPorMes)maximo 
    FROM (
      SELECT MONTH(f.fecha_descarga)mes, COUNT(*)descargasPorMes 
        FROM fechadescarga f 
        GROUP BY MONTH(f.fecha_descarga)
    ) c1;

  /************
    CON JOIN
  ************/
  -- Consulta final
  SELECT c1.mes 
    FROM (
      SELECT MONTH(f.fecha_descarga)mes, COUNT(*)descargasPorMes 
        FROM fechadescarga f 
        GROUP BY MONTH(f.fecha_descarga)
    )c1 
    JOIN (
      SELECT MAX(c1.descargasPorMes)maximo 
        FROM (
          SELECT MONTH(f.fecha_descarga)mes, COUNT(*)descargasPorMes 
            FROM fechadescarga f 
            GROUP BY MONTH(f.fecha_descarga)
        ) c1
    ) c2 
    ON c1.descargasPorMes = c2.maximo;

  /*************
    CON HAVING
  *************/
  -- Consulta final
  SELECT MONTH(f.fecha_descarga)mes, COUNT(*)descargasPorMes 
    FROM fechadescarga f 
    GROUP BY MONTH(f.fecha_descarga)
    HAVING descargasPorMes=( 
      SELECT MAX(c1.descargasPorMes)maximo 
        FROM (
          SELECT MONTH(f.fecha_descarga)mes, COUNT(*)descargasPorMes 
            FROM fechadescarga f 
            GROUP BY MONTH(f.fecha_descarga)
        ) 
    c1);