﻿USE concursos;

-- Indicar el nombre de los concursantes cuya altura sea 170 o mas

  SELECT 
    DISTINCT c.nombre 
  FROM 
    concursantes c 
  WHERE 
    c.altura>=170;

-- Indicar los pesos que aparecen más de 1 vez

  SELECT 
    c.peso 
  FROM 
    concursantes c 
  GROUP BY 
    c.peso 
  HAVING 
    COUNT(*)>1;

-- Indicar las poblaciones con más de 3 concursantes que hayna nacido despues de 1983

  SELECT 
    c.poblacion 
  FROM 
    concursantes c 
  WHERE 
    YEAR(c.fechaNacimiento)>1983 
  GROUP BY 
    c.poblacion 
  HAVING 
    COUNT(*)>3;

-- Indicar la altura media y el peso maximo de los concursantes por provincia

  SELECT 
    c.provincia ,AVG(c.altura)altura_media, MAX(c.peso)peso_maximo 
  FROM 
    concursantes c
  GROUP BY
    c.provincia;

-- Indicar la provincia que tiene 5 concursantes o más de peso entre 95 y 105 y mostrar el peso maximo y el minimo

  SELECT 
    c.provincia, MAX(c.peso)peso_maximo, MIN(c.peso)peso_minimo 
  FROM 
    concursantes c 
  WHERE 
    c.peso BETWEEN 95 AND 105 
  GROUP BY 
    c.provincia 
  HAVING 
    COUNT(*)>=5;
