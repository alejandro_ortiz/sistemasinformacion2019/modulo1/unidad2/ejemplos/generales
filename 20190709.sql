﻿USE concursos;

-- 1. Listar nombre cuya altura sea mayor de 170 cm

  SELECT 
    DISTINCT c.nombre 
  FROM 
    concursantes c 
  WHERE 
    c.altura>170;

-- 2. Listame las poblaciones donde los concursantes pesen menos de 90 kg

  SELECT 
    DISTINCT c.poblacion 
  FROM 
    concursantes c 
  WHERE 
    c.peso<90;

-- 3. Listame las poblaciones que tengan más de 5 concursantes con mas de 90 kg

  SELECT 
    c.poblacion 
  FROM 
    concursantes c 
  WHERE 
    c.peso>90 
  GROUP BY 
    c.poblacion 
  HAVING 
    COUNT(*)>=5;

-- 4. Saca la media de altura y altura máxima de los concursantes

  SELECT 
    AVG(c.altura)altura_media, MAX(c.altura)altura_maxima 
  FROM 
    concursantes c;

-- 5. Media de peso y peso maximo por poblacion de cantabria 

  SELECT 
    c.poblacion, AVG(c.peso)peso_medio, MAX(c.peso)peso_medio 
  FROM 
    concursantes c 
  WHERE 
    c.provincia='Cantabria' 
  GROUP BY c.poblacion; 
