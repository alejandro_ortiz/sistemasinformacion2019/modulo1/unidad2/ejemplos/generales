﻿/** Consultaas sobre la base de datos pesca **/

USE pesca;

-- 1. Indica el nombre del club que tiene pescadores

  SELECT DISTINCT c.nombre 
    FROM clubes c 
    JOIN pescadores p 
    ON c.cif = p.club_cif;

-- 2. Indica el nombre del club que no tiene pescadores

  SELECT c.nombre 
    FROM clubes c 
    LEFT JOIN pescadores p 
    ON c.cif = p.club_cif 
    WHERE p.club_cif IS NULL;

-- 3. Indica el nombre de los cotos autorizados a algún club

  SELECT DISTINCT a.coto_nombre 
    FROM autorizados a;

-- 4. Indica la provincia que tiene cotos autorizados a algún club

  SELECT DISTINCT c.provincia 
    FROM cotos c 
    JOIN autorizados a 
    ON c.nombre = a.coto_nombre;

-- 5. Indica el nombre de los cotos que no están autorizados a ningún club

  SELECT c.nombre 
    FROM cotos c 
    LEFT JOIN autorizados a 
    ON c.nombre = a.coto_nombre
    WHERE a.coto_nombre IS NULL;

-- 6. Indica el número de ríos por provincia con cotos

  SELECT c.provincia, COUNT(DISTINCT c.rio)numeroRios 
    FROM cotos c 
    GROUP BY c.provincia;

-- 7. Indica el número de ríos por provincia con cotos autorizados

  SELECT c.provincia, COUNT(DISTINCT c.rio)numeroRios 
    FROM cotos c 
    JOIN autorizados a 
    ON c.nombre = a.coto_nombre 
    GROUP BY c.provincia;

-- 8. Indica el nombre de la provincia con más cotos autorizados

  -- Subconsulta C1
  -- Numero de cotos autorizados por provincia
  SELECT c.provincia, COUNT(DISTINCT c.nombre)numCotos 
    FROM cotos c 
    JOIN autorizados a 
    ON c.nombre = a.coto_nombre 
    GROUP BY c.provincia;

  -- Subconsulta C2
  -- Numero maximo de cotos autorizados por provincia 
  SELECT MAX(C1.numCotos)maximo 
    FROM (
      SELECT c.provincia, COUNT(DISTINCT c.nombre)numCotos 
        FROM cotos c 
        JOIN autorizados a 
        ON c.nombre = a.coto_nombre 
        GROUP BY c.provincia
    ) C1;

  -- Consulta final
  SELECT C1.provincia 
    FROM (
      SELECT c.provincia, COUNT(DISTINCT c.nombre)numCotos 
        FROM cotos c 
        JOIN autorizados a 
        ON c.nombre = a.coto_nombre 
        GROUP BY c.provincia
    ) C1 
    JOIN (
      SELECT MAX(C1.numCotos)maximo 
        FROM (
          SELECT c.provincia, COUNT(DISTINCT c.nombre)numCotos 
            FROM cotos c 
            JOIN autorizados a 
            ON c.nombre = a.coto_nombre 
            GROUP BY c.provincia
        ) C1
    ) C2 
    ON C1.numCotos = C2.maximo;

-- 9. Indica el nombre del pescador y el nombre de su ahijado

  SELECT padrinos.nombre AS padrino, ahijados.nombre AS ahijado 
    FROM pescadores padrinos 
    JOIN apadrinar a ON padrinos.numSocio = a.padrino 
    JOIN pescadores ahijados ON a.ahijado = ahijados.numSocio;

-- 10. Indica el número de ahijados de cada pescador

  SELECT a.padrino, COUNT(*)numAhijados 
    FROM apadrinar a 
    GROUP BY a.padrino;
