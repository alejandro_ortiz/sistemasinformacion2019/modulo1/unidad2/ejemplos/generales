﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 23/07/2019 10:02:17
-- Server version: 5.5.5-10.1.40-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

-- 
-- Dumping data for table cotos
--
INSERT INTO cotos VALUES
('Ark', 'Mississippi', 'Attleborough'),
('Arkadelphia', 'Mississippi', 'Thornton Heath'),
('Arkansas City', 'Vermont', 'Isle of Mull'),
('Arlee', 'New Mexico', 'Cirencester'),
('Arlington', 'Idaho', 'Egham'),
('Arlington Heights', 'Maine', 'Worcester Park'),
('Armada', 'Pennsylvania', 'Newark'),
('Canyonville', 'Montana', 'Brierley Hill'),
('Capac', 'Maine', 'Isle of Islay'),
('Cape Canaveral', 'New Mexico', 'Brighouse'),
('Cape Charles', 'New Jersey', 'Longhope'),
('Cape Coral', 'Alaska', 'Tidworth'),
('Cape Elizabeth', 'Alaska', 'Isle of Skye'),
('Cape Girardeau', 'Rhode Island', 'Tiverton'),
('Eastpoint', 'Mississippi', 'Wood Green'),
('Eastpointe', 'Wyoming', 'Woodford Green'),
('Eastport', 'Wisconsin', 'Edenbridge'),
('Eastsound', 'Connecticut', 'Brighton'),
('Eaton', 'California', 'Edgware'),
('Eaton County', 'Delaware', 'New Milton'),
('Eaton Rapids', 'Arkansas', 'Richmond'),
('Eatonton', 'Mississippi', 'New Romney'),
('Eatontown', 'Arizona', 'Tintagel'),
('Eatonville', 'North Dakota', 'Lostwithiel'),
('Greenbelt', 'West Virginia', 'East Molesey'),
('Greenbrae', 'Maine', 'Lockerbie'),
('Greenbrier', 'Wyoming', 'Chulmleigh'),
('Greenbush', 'South Dakota', 'Aughnacloy'),
('Greencastle', 'Nebraska', 'Longniddry'),
('Greendale', 'Massachusetts', 'Axbridge'),
('Greene', 'Idaho', 'Tillicoultry'),
('Greeneville', 'Indiana', 'Rickmansworth'),
('Greenfield', 'Florida', 'Aylesbury'),
('Greenland', 'Nevada', 'Egremont'),
('Greenlawn', 'Alabama', 'New Southgate'),
('Leflore', 'Vermont', 'Thornton-Cleveleys'),
('Leggett', 'New York', 'Brigg'),
('Lehi', 'Michigan', 'Rhyl'),
('Lehigh', 'Nebraska', 'Eastleigh'),
('Lehigh Acres', 'Minnesota', 'Church Stretton'),
('Lehigh Valley', 'Washington', 'Isle of Portland'),
('Lehighton', 'Kansas', 'Tilbury'),
('Lehman', 'Florida', 'Bristol'),
('Lehr', 'Pennsylvania', 'New Quay'),
('Leicester', 'Florida', 'Axminster'),
('Leitchfield', 'Texas', 'Isle of Scalpay'),
('Leiters Ford', 'Michigan', 'Woolwich'),
('Leland', 'Vermont', 'New Tredegar'),
('Morrow', 'Delaware', 'Woodford'),
('Morse', 'Mississippi', 'Auchterarder'),
('Morton', 'Utah', 'Thurso'),
('Morton Grove', 'Connecticut', 'Longfield'),
('Mosca', 'California', 'Ebbw Vale'),
('Moses Lake', 'Georgia', 'Richmond'),
('Mosier', 'South Carolina', 'Edinburgh'),
('Mosinee', 'Connecticut', 'Wooler'),
('Mosquero', 'Utah', 'Clapham'),
('Moss', 'Georgia', 'Brixton'),
('Moss Beach', 'Nebraska', 'Lossiemouth'),
('Moss Landing', 'Tennessee', 'Worcester'),
('Moss Point', 'Utah', 'Loughborough'),
('Oakes', 'Minnesota', 'Lochwinnoch'),
('Oakesdale', 'Mississippi', 'Chryston'),
('Oakfield', 'New York', 'Thorpe Hall'),
('Oakham', 'Kansas', 'Londonderry [2]'),
('Oakhurst', 'Kentucky', 'Cinderford'),
('Oakland', 'Iowa', 'Brighton'),
('Oakland City', 'Arkansas', 'Isle of South Uist'),
('Oakland Gardens', 'New Hampshire', 'Ringwood'),
('Reedsburg', 'Wyoming', 'Isle of Jura'),
('Reedsport', 'Oklahoma', 'Eastbourne'),
('Reedsville', 'Nebraska', 'Isle of Lewis'),
('Reese', 'Idaho', 'Brighton'),
('Reform', 'Delaware', 'Aviemore'),
('Refugio', 'Nevada', 'Isle of Rhum'),
('Regent', 'Kentucky', 'Broadford'),
('Rego Park', 'Nebraska', 'Isle of Tiree'),
('Selbyville', 'Missouri', 'Ystrad Meurig'),
('Selden', 'Delaware', 'Woodbridge'),
('Selfridge', 'Michigan', 'New Cross'),
('Seligman', 'Georgia', 'Isle of North Uist'),
('Selinsgrove', 'Ohio', 'Woodhall Spa'),
('Selkirk', 'Washington', 'Tighnabruaich'),
('Sellersburg', 'Connecticut', 'Avoch'),
('Sellersville', 'Arkansas', 'Looe'),
('Sells', 'Virginia', 'Brixham'),
('Selma', 'Missouri', 'Tipton'),
('Selmer', 'Florida', 'Clapton'),
('Truman', 'Louisiana', 'Thornliebank'),
('Trumann', 'Delaware', 'Rhosneigr'),
('Trumansburg', 'Arkansas', 'Augher'),
('Trumbull', 'New Hampshire', 'New Malden'),
('Trumbull County', 'Oklahoma', 'Woodstock'),
('Truro', 'Michigan', 'Woolacombe'),
('Trussville', 'Indiana', 'Clackmannan'),
('Truth Or Consequences', 'Illinois', 'Clacton-on-Sea'),
('Tryon', 'Massachusetts', 'Riding Mill'),
('Tualatin', 'Mississippi', 'Ripley'),
('Tuba City', 'Delaware', 'Aylesford'),
('Zuni', 'Arkansas', 'Atherstone');

-- 
-- Dumping data for table clubes
--
INSERT INTO clubes VALUES
('016506573', 'Federal Natural Gas Energy Corporation', 'Nevada'),
('033320846', 'Special High-Technologies Corporation', 'Vermont'),
('068855951', 'First Management Group', 'Vermont'),
('075270473', 'International Telecom Inc.', 'New Hampshire'),
('078044504', 'United Y-Mobile Inc.', 'Florida'),
('083373080', 'Canadian Nuclear Resources Corporation', 'New York'),
('096713787', 'Smart High-Technologies Inc.', 'Alaska'),
('114876322', 'Flexible 2G Wireless Inc.', 'Wyoming'),
('117961252', 'Smart Services Inc.', 'Maine'),
('128847813', 'National Renewable Resources Corporation', 'North Carolina'),
('131045156', 'Domestic Renewable Resources Group', 'Hawaii'),
('142610350', 'General Space Research Group', 'New York'),
('168961736', 'Australian D-Mobile Co.', 'Pennsylvania'),
('170552317', 'Domestic Green Energy Group', 'Tennessee'),
('171674698', 'Federal High-Technologies Corp.', 'Idaho'),
('174265453', 'Flexible Investment Co.', 'Georgia'),
('210321064', 'General High-Technologies Co.', 'California'),
('213476021', 'Beyond Space Research Corporation', 'Arkansas'),
('218568408', 'North Delivery Corporation', 'Delaware'),
('221163934', 'Australian 3G Wireless Group', 'Arkansas'),
('230795177', 'Smart Products Corporation', 'South Carolina'),
('239969158', 'City High-Technologies Corporation', 'Colorado'),
('257587901', 'Professional Space Explore Group', 'Mississippi'),
('272288627', 'Canadian Materials Corp.', 'Kentucky'),
('277681557', 'West Laboratories Group', 'New Hampshire'),
('301142855', 'General Mobile Group', 'Wisconsin'),
('304313763', 'Pacific Space Explore Group', 'Alaska'),
('305970237', 'East G-Mobile Group', 'New Hampshire'),
('327443037', 'Australian Telecommunications Group', 'Alaska'),
('358123686', 'Global Y-Mobile Corporation', 'Minnesota'),
('362816719', 'Home 2G Wireless Inc.', 'Utah'),
('395510053', 'Professional Telemetrics Inc.', 'Georgia'),
('430890992', 'Pacific Automotive Group', 'Ohio'),
('437974265', 'West Coast Automotive Group', 'Washington'),
('449180782', 'National J-Mobile Corporation', 'Alabama'),
('457513662', 'Domestic Green Resources Inc.', 'Delaware'),
('462711922', 'Professional Z-Mobile Inc.', 'Nevada'),
('495101931', 'South Space Explore Corporation', 'West Virginia'),
('496193984', 'International Space Research Co.', 'Arizona'),
('503743554', 'North K-Mobile Corporation', 'Michigan'),
('511343801', 'United I-Mobile Co.', 'North Carolina'),
('517205871', 'National Space Explore Corporation', 'North Dakota'),
('518192918', 'Professional Solar Resources Corp.', 'Georgia'),
('521700787', 'West Coast Electricity Inc.', 'Washington'),
('524523608', 'South Y-Mobile Corporation', 'Arkansas'),
('531295702', 'International Space Explore Inc.', 'Wyoming'),
('554722829', 'Global High-Technologies Inc.', 'Texas'),
('555133489', 'American Solar Resources Corporation', 'Illinois'),
('567292462', 'First Management Inc.', 'North Dakota'),
('579706332', 'Federal Software Inc.', 'Idaho'),
('580548062', 'Professional High-Technologies Group', 'California'),
('583571246', 'Canadian Space Research Inc.', 'New Hampshire'),
('587140155', 'General Computers Corporation', 'Idaho'),
('635289190', 'Special Equipment Corp.', 'Kansas'),
('636117751', 'Global 5G Wireless Corporation', 'Washington'),
('640078114', 'Smart 3G Wireless Corporation', 'Nevada'),
('644969468', 'Pacific Optics Group', 'Vermont'),
('654410709', 'WorldWide 5G Wireless Inc.', 'Nebraska'),
('655076857', 'United Transport Corporation', 'Oklahoma'),
('671656308', 'Union Space Explore Group', 'Vermont'),
('700497950', 'East High-Technologies Group', 'Connecticut'),
('701037002', 'Professional Heating Corp.', 'Texas'),
('717552971', 'Home R-Mobile Corporation', 'Kansas'),
('719074611', 'West Space Research Corporation', 'Connecticut'),
('732643829', 'United 4G Wireless Inc.', 'Ohio'),
('735432168', 'International High-Technologies Group', 'Washington'),
('750578337', 'Western I-Mobile Co.', 'Texas'),
('777837220', 'North Logistic Inc.', 'Georgia'),
('780929346', 'North 3G Wireless Corporation', 'Wyoming'),
('786048320', 'Flexible Space Explore Corp.', 'Alabama'),
('790942750', 'Smart 4G Wireless Group', 'New York'),
('796410674', 'Flexible High-Technologies Corporation', 'South Carolina'),
('807376736', 'Canadian Space Explore Group', 'Washington'),
('810276730', 'Global Industry Corporation', 'Delaware'),
('814436066', 'Australian High-Technologies Corp.', 'Rhode Island'),
('829626233', 'Domestic 3G Wireless Group', 'Delaware'),
('851705487', 'Home Y-Mobile Group', 'Arizona'),
('855856477', 'International Goods Co.', 'Oklahoma'),
('857969269', 'Domestic K-Mobile Inc.', 'Illinois'),
('863624325', 'Federal High-Technologies Inc.', 'Florida'),
('865655066', 'Domestic High-Technologies Inc.', 'California'),
('867286750', 'Western Consulting Inc.', 'Minnesota'),
('867723213', 'Global I-Mobile Corporation', 'Maine'),
('868381941', 'South High-Technologies Corporation', 'Kansas'),
('876254214', 'Smart Space Research Co.', 'Idaho'),
('879626603', 'Home Optics Inc.', 'New Jersey'),
('886972339', 'Creative L-Mobile Inc.', 'Utah'),
('889573284', 'South Materials Group', 'Texas'),
('905632272', 'East High-Technologies Group', 'West Virginia'),
('921032618', 'American O-Mobile Inc.', 'Maine'),
('926945411', 'First Services Inc.', 'Minnesota'),
('933935676', 'East Nuclear Energy Group', 'Maryland'),
('943397200', 'Domestic Space Research Inc.', 'Mississippi'),
('947577106', 'Pacific Space Explore Inc.', 'Delaware'),
('959080732', 'City Space Research Corporation', 'New York'),
('961825082', 'Australian Systems Corp.', 'Tennessee'),
('973911706', 'Flexible K-Mobile Inc.', 'Connecticut'),
('980252672', 'First M-Mobile Corporation', 'Alabama'),
('981724817', 'Canadian High-Technologies Corp.', 'North Dakota'),
('986466600', 'Domestic Services Group', 'Florida');

-- 
-- Dumping data for table pescadores
--
INSERT INTO pescadores VALUES
(665485, '65618164K', 'Hadassa', 'Amarillo', '1988-02-05', '867286750'),
(665486, '19070404K', 'Milvina', 'Verde', '2003-09-10', '257587901'),
(665487, '94945928T', 'Esthera', 'Verde', '2009-10-16', '517205871'),
(665488, '20835653P', 'Eckehard', 'Rojo', '1970-01-30', '555133489'),
(665489, '54316835E', 'Ravissa', 'Rojo', '1971-12-03', '719074611'),
(665490, '55434739D', 'Hunfried', 'Verde', '1979-03-25', '635289190'),
(665491, '85115104K', 'Clivia', 'Amarillo', '1970-01-10', '583571246'),
(665492, '42405787Q', 'Waltraud', 'Rojo', '2016-08-19', '868381941'),
(665493, '36842390P', 'Titus', 'Rojo', '2000-01-11', '171674698'),
(1665486, '26109504K', 'Vinia', 'Rojo', '1985-01-04', '863624325'),
(1665487, '61671500N', 'Jouleen', 'Rojo', '1970-10-28', '579706332'),
(1665488, '62842798A', 'Hrodeberht', 'Amarillo', '1989-05-18', '655076857'),
(1665489, '35084430Q', 'Urias', 'Amarillo', '2014-02-25', '449180782'),
(1665490, '35140825T', 'Aurion ', 'Rojo', '1970-06-01', '272288627'),
(1665491, '02588520V', 'Annea', 'Amarillo', '1971-07-07', '807376736'),
(1665492, '30540372X', 'Marell', 'Verde', '1971-07-17', '170552317'),
(1665493, '31684506X', 'Elayla', 'Amarillo', '1991-12-22', '580548062'),
(2204806, '23883280F', 'Herlinde', 'Amarillo', '1971-08-12', '170552317'),
(2204807, '91791592A', 'Achime ', 'Amarillo', '1970-04-10', '777837220'),
(2204808, '61152401E', 'Lando', 'Amarillo', '1970-01-10', '503743554'),
(2204809, '52760347R', 'Janaida', 'Amarillo', '1970-04-14', '973911706'),
(2204810, '25558703M', 'Kreszenz', 'Verde', '1972-07-01', '168961736'),
(2204811, '19118338D', 'Uli', 'Rojo', '1986-05-06', '851705487'),
(2204812, '63355666Q', 'Sixtus', 'Rojo', '1970-01-03', '567292462'),
(2204813, '66346663C', 'Emme', 'Rojo', '1976-04-22', '807376736'),
(2204814, '79961355B', 'Aliciana ', 'Verde', '1970-01-01', '068855951'),
(3204806, '02240473X', 'Edeltrude', 'Verde', '1986-02-01', '863624325'),
(3204807, '68477508J', 'Ottokar', 'Verde', '1990-12-17', '301142855'),
(3204808, '51668803T', 'Helmtraud', 'Amarillo', '1971-02-01', '867286750'),
(3204809, '51603523S', 'Ilai', 'Amarillo', '1982-04-01', '305970237'),
(3204810, '54120022J', 'Winrich', 'Amarillo', '1970-05-28', '117961252'),
(3204811, '58822754G', 'Wilfrieda', 'Amarillo', '1989-08-20', '358123686'),
(3204812, '99017946H', 'Naika', 'Rojo', '2012-11-28', '583571246'),
(3204813, '35605350A', 'Sybille', 'Verde', '1970-01-05', '973911706'),
(3204814, '78581554C', 'Yoda', 'Amarillo', '2006-10-28', '857969269'),
(4706026, '37225192P', 'Baldo ', 'Verde', '2012-03-11', '395510053'),
(4706027, '88731400K', 'Loritta', 'Rojo', '1985-07-24', '327443037'),
(4706028, '77752982L', 'Farin', 'Rojo', '1993-11-13', '640078114'),
(4706029, '99806911T', 'Lenn', 'Rojo', '1973-09-12', '867286750'),
(4706030, '65748337W', 'Otfried', 'Rojo', '1970-02-09', '905632272'),
(4706031, '49207336L', 'Jolinda', 'Rojo', '2000-12-01', '016506573'),
(4706032, '75062857E', 'Coloman', 'Verde', '2017-10-08', '926945411'),
(4706033, '41470969A', 'Bero', 'Rojo', '2014-10-13', '905632272'),
(4706034, '38527692K', 'Markolf', 'Verde', '2014-04-11', '305970237'),
(4706035, '54493722D', 'Cherina', 'Rojo', '1970-02-22', '362816719'),
(4706036, '96677032P', 'Lenhard', 'Rojo', '1970-03-05', '700497950'),
(5706027, '06829079R', 'Urban', 'Verde', '1970-01-04', '829626233'),
(5706028, '80815921Y', 'Rabanus', 'Rojo', '1998-04-10', '114876322'),
(5706029, '21622335R', 'Mommo', 'Verde', '2015-09-14', '362816719'),
(5706030, '33959172X', 'Balte ', 'Verde', '1970-01-01', '327443037'),
(5706031, '21884696R', 'Meret', 'Amarillo', '1977-10-06', '579706332'),
(5706032, '93963992W', 'Gardi', 'Rojo', '2009-03-08', '078044504'),
(5706033, '89533068R', 'Conny', 'Verde', '1972-02-28', '654410709'),
(5706034, '86963312H', 'Markwart', 'Rojo', '1972-06-01', '437974265'),
(5706035, '93937873Z', 'Marla', 'Verde', '1986-07-20', '033320846'),
(5706036, '44165895R', 'Yascha', 'Amarillo', '1981-08-27', '814436066'),
(6409379, '86985538M', 'Tizian', 'Rojo', '1992-10-11', '430890992'),
(6409380, '21270539B', 'Cathrine', 'Amarillo', '2008-10-17', '980252672'),
(6409381, '41548693T', 'Siw', 'Amarillo', '1970-02-27', '503743554'),
(6409382, '63033790R', 'Dianara', 'Amarillo', '1970-11-27', '218568408'),
(6409383, '63433741Y', 'Hajo', 'Verde', '1984-11-06', '171674698'),
(6409384, '61549909Y', 'Seraphim', 'Rojo', '1985-06-11', '554722829'),
(6409385, '63798541X', 'Dorabella', 'Amarillo', '1998-08-03', '921032618'),
(6409386, '62476657K', 'Gonzales', 'Verde', '2017-12-05', '555133489'),
(6409387, '79594417R', 'Katjana', 'Rojo', '1971-06-21', '437974265'),
(6409388, '71781820A', 'Lando', 'Verde', '1970-05-07', '732643829'),
(7409380, '65452535D', 'Balduin ', 'Rojo', '1986-06-18', '777837220'),
(7409381, '56709496N', 'Emme', 'Verde', '2000-02-24', '457513662'),
(7409382, '88857743D', 'Gabriele', 'Verde', '1997-08-08', '075270473'),
(7409383, '96073109G', 'Aleida ', 'Rojo', '2010-02-19', '075270473'),
(7409384, '95966086A', 'Philina', 'Verde', '1970-03-22', '732643829'),
(7409385, '98507463C', 'Annaliese', 'Verde', '1985-06-12', '524523608'),
(7409386, '68026982H', 'Friederika', 'Amarillo', '1995-09-23', '851705487'),
(7409387, '72636623D', 'Thomia', 'Amarillo', '1970-04-08', '717552971'),
(7409388, '90371766D', 'Sendrik', 'Rojo', '1974-04-04', '555133489'),
(8462469, '82341356S', 'Hella', 'Amarillo', '1970-01-02', '257587901'),
(8462470, '03451542M', 'Uschi', 'Verde', '2007-04-13', '272288627'),
(8462471, '49157519A', 'Eginhard', 'Rojo', '1993-07-31', '554722829'),
(8462472, '14554472S', 'Lisiane', 'Rojo', '1970-01-08', '796410674'),
(8462473, '30806640J', 'Nussi', 'Verde', '2019-06-08', '807376736'),
(8462474, '20962987T', 'Lyla', 'Amarillo', '1978-06-05', '654410709'),
(8462475, '47897211B', 'Gustav', 'Rojo', '2006-12-29', '865655066'),
(8462476, '21874854B', 'Eitel-Fritz', 'Verde', '1970-01-07', '362816719'),
(8462477, '28008492C', 'Petrissa', 'Amarillo', '2004-07-02', '301142855'),
(8462478, '80176949Y', 'Normanicus', 'Amarillo', '2002-01-31', '518192918'),
(8462479, '63476510Y', 'Modena', 'Verde', '1983-10-25', '961825082'),
(8462480, '05180781A', 'Christoph', 'Amarillo', '2013-10-16', '170552317'),
(8462481, '52022890T', 'Ysett', 'Rojo', '2004-01-06', '221163934'),
(9462467, '42091159C', 'Marlyse', 'Amarillo', '2017-11-06', '981724817'),
(9462468, '23395678H', 'Xaver', 'Amarillo', '1972-03-03', '531295702'),
(9462469, '90286963D', 'Arlett', 'Rojo', '1972-09-19', '171674698'),
(9462470, '14171021T', 'Bernfried', 'Rojo', '2009-09-02', '117961252'),
(9462471, '23007948N', 'Enrike', 'Amarillo', '1988-06-27', '503743554'),
(9462472, '25260404V', 'Kaleb', 'Verde', '1993-02-26', '277681557'),
(9462473, '32679137X', 'Briska', 'Verde', '1991-09-04', '171674698'),
(9462474, '22951037T', 'Jodokus', 'Verde', '1970-03-07', '879626603'),
(9462475, '90387134K', 'Roho', 'Amarillo', '2012-01-04', '131045156'),
(9462476, '21489855F', 'Edeltraud', 'Amarillo', '1989-03-12', '449180782'),
(9462477, '92349505X', 'Jodok', 'Verde', '1970-01-20', '796410674'),
(9462478, '97596050Z', 'Wunibald', 'Verde', '1972-05-25', '327443037');

-- 
-- Dumping data for table autorizados
--
INSERT INTO autorizados VALUES
('016506573', 'Arlington Heights'),
('033320846', 'Trumansburg'),
('068855951', 'Selma'),
('075270473', 'Capac'),
('078044504', 'Greendale'),
('083373080', 'Lehigh Valley'),
('096713787', 'Cape Elizabeth'),
('114876322', 'Greene'),
('117961252', 'Mosier'),
('128847813', 'Eastpoint'),
('131045156', 'Selmer'),
('142610350', 'Greenbrae'),
('168961736', 'Eastport'),
('170552317', 'Selden'),
('171674698', 'Reedsville'),
('174265453', 'Mosca'),
('210321064', 'Trumbull County'),
('213476021', 'Eatonton'),
('218568408', 'Mosinee'),
('221163934', 'Leicester'),
('230795177', 'Reform'),
('239969158', 'Eaton'),
('257587901', 'Trumann'),
('272288627', 'Truth Or Consequences'),
('277681557', 'Lehi'),
('301142855', 'Tualatin'),
('304313763', 'Armada'),
('305970237', 'Sellersburg'),
('327443037', 'Morse'),
('358123686', 'Reedsport'),
('362816719', 'Morton Grove'),
('395510053', 'Selbyville'),
('430890992', 'Cape Girardeau'),
('437974265', 'Lehighton'),
('449180782', 'Greeneville'),
('457513662', 'Lehigh'),
('462711922', 'Eaton County'),
('495101931', 'Oakfield'),
('496193984', 'Oakham'),
('503743554', 'Ark'),
('511343801', 'Morton'),
('517205871', 'Tuba City'),
('518192918', 'Eatonville'),
('521700787', 'Lehman'),
('524523608', 'Moss Beach'),
('531295702', 'Reedsburg'),
('554722829', 'Leland'),
('555133489', 'Sellersville'),
('567292462', 'Leflore'),
('579706332', 'Greenbelt'),
('580548062', 'Cape Coral'),
('583571246', 'Truman'),
('587140155', 'Oakesdale'),
('635289190', 'Arlee'),
('636117751', 'Zuni'),
('640078114', 'Seligman'),
('644969468', 'Regent'),
('654410709', 'Tryon'),
('655076857', 'Selfridge'),
('671656308', 'Moss Landing'),
('700497950', 'Greenlawn'),
('701037002', 'Greencastle'),
('717552971', 'Moss'),
('719074611', 'Oakhurst'),
('732643829', 'Arkansas City'),
('735432168', 'Selinsgrove'),
('750578337', 'Rego Park'),
('777837220', 'Oakes'),
('780929346', 'Eatontown'),
('786048320', 'Leiters Ford'),
('790942750', 'Cape Charles'),
('796410674', 'Eastsound'),
('807376736', 'Moss Point'),
('810276730', 'Leitchfield'),
('814436066', 'Leggett'),
('829626233', 'Lehigh Acres'),
('851705487', 'Refugio'),
('855856477', 'Oakland Gardens'),
('857969269', 'Moses Lake'),
('863624325', 'Lehr'),
('865655066', 'Oakland'),
('867286750', 'Greenfield'),
('867723213', 'Trumbull'),
('868381941', 'Oakland City'),
('876254214', 'Greenbrier'),
('879626603', 'Eastpointe'),
('886972339', 'Arlington'),
('889573284', 'Canyonville'),
('905632272', 'Trussville'),
('921032618', 'Morrow'),
('926945411', 'Selkirk'),
('933935676', 'Greenland'),
('943397200', 'Cape Canaveral'),
('947577106', 'Reese'),
('959080732', 'Sells'),
('961825082', 'Arkadelphia'),
('973911706', 'Eaton Rapids'),
('980252672', 'Truro'),
('981724817', 'Mosquero'),
('986466600', 'Greenbush');

-- 
-- Dumping data for table apadrinar
--
INSERT INTO apadrinar VALUES
(665485, 665485),
(665486, 665486),
(665487, 665487),
(665488, 665488),
(665489, 665489),
(665490, 665490),
(665491, 665491),
(665492, 665492),
(665493, 665493),
(1665486, 1665486),
(1665487, 1665487),
(1665488, 1665488),
(1665489, 1665489),
(1665490, 1665490),
(1665491, 1665491),
(1665492, 1665492),
(1665493, 1665493),
(2204806, 2204806),
(2204807, 2204807),
(2204808, 2204808),
(2204809, 2204809),
(2204810, 2204810),
(2204811, 2204811),
(2204812, 2204812),
(2204813, 2204813),
(2204814, 2204814),
(3204806, 3204806),
(3204807, 3204807),
(3204808, 3204808),
(3204809, 3204809),
(3204810, 3204810),
(3204811, 3204811),
(3204812, 3204812),
(3204813, 3204813),
(3204814, 3204814),
(4706026, 4706026),
(4706027, 4706027),
(4706028, 4706028),
(4706029, 4706029),
(4706030, 4706030),
(4706031, 4706031),
(4706032, 4706032),
(4706033, 4706033),
(4706034, 4706034),
(4706035, 4706035),
(4706036, 4706036),
(5706027, 5706027),
(5706028, 5706028),
(5706029, 5706029),
(5706030, 5706030),
(5706031, 5706031),
(5706032, 5706032),
(5706033, 5706033),
(5706034, 5706034),
(5706035, 5706035),
(5706036, 5706036),
(6409379, 6409379),
(6409380, 6409380),
(6409381, 6409381),
(6409382, 6409382),
(6409383, 6409383),
(6409384, 6409384),
(6409385, 6409385),
(6409386, 6409386),
(6409387, 6409387),
(6409388, 6409388),
(7409380, 7409380),
(7409381, 7409381),
(7409382, 7409382),
(7409383, 7409383),
(7409384, 7409384),
(7409385, 7409385),
(7409386, 7409386),
(7409387, 7409387),
(7409388, 7409388),
(8462469, 8462469),
(8462470, 8462470),
(8462471, 8462471),
(8462472, 8462472),
(8462473, 8462473),
(8462474, 8462474),
(8462475, 8462475),
(8462476, 8462476),
(8462477, 8462477),
(8462478, 8462478),
(8462479, 8462479),
(8462480, 8462480),
(8462481, 8462481),
(9462467, 9462467),
(9462468, 9462468),
(9462469, 9462469),
(9462470, 9462470),
(9462471, 9462471),
(9462472, 9462472),
(9462473, 9462473),
(9462474, 9462474),
(9462475, 9462475),
(9462476, 9462476),
(9462477, 9462477),
(9462478, 9462478);

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;