﻿USE ciclistas;

-- Numero de etapas que hay

  SELECT COUNT(*) total_etapas FROM etapa e; 

-- Numero de maillots que hay

  SELECT COUNT(*) total_maillots FROM maillot m;

-- Numero de ciclistas que hayan ganado alguna etapa

-- con subconsulas
  -- ganadores: dorsal de los ciclistas que han ganado una etapa
  SELECT DISTINCT e.dorsal FROM etapa e;

  SELECT COUNT(*) ciclistas_ganadores_etapa  
    FROM (SELECT DISTINCT e.dorsal FROM etapa e) ganadores; 


-- sin subconsultas

  SELECT COUNT(DISTINCT e.dorsal) ciclistas_ganadores_etapa FROM etapa e;