﻿USE ciclistas;

/**
  ciclistas que han ganado etapas
  mostrar doral
*/

  SELECT DISTINCT e.dorsal FROM etapa e;

/**
  ciclistas que han ganado puertos
  mostrar doral
*/

  SELECT DISTINCT p.dorsal FROM puerto p;

/**
  dorsal de ciclistas que han ganado etapas o puertos
*/
 
  SELECT DISTINCT e.dorsal FROM etapa e
    UNION
  SELECT DISTINCT p.dorsal FROM puerto p;

/**
  dorsal de ciclistas que han ganado etapas o puertos
*/

  /**  
  No funciona porque no soporta el operador INTERSECT para la interseccion
  SELECT DISTINCT e.dorsal FROM etapa e
    INTERSECT
  SELECT DISTINCT p.dorsal FROM puerto p;
  */

   -- Con operador de combinacion JOIN
  SELECT c1.dorsalEtapa dorsal FROM 
    (SELECT DISTINCT e.dorsal dorsalEtapa FROM etapa e) c1
    JOIN
    (SELECT DISTINCT p.dorsal dorsalPuerto FROM puerto p) c2
    ON c1.dorsalEtapa = c2.dorsalPuerto;
 
  -- Listado de todas las etapas y el ciclista que las ha ganado
  SELECT * 
    FROM ciclista c JOIN etapa e 
    ON c.dorsal = e.dorsal;

  -- Listado de todos los puertos y el ciclista que los ha coronasdo primero
  SELECT * 
    FROM ciclista c JOIN puerto p 
    ON c.dorsal = p.dorsal;   

/**
  combinacion interna   
*/
 
  -- Listar todos los ciclistas con todos los datos del equipo al que pertenecen
  -- SELECT * FROM equipo e INNER JOIN -- el INNER es opcional y no lo ponemos 
  SELECT * 
    FROM equipo e JOIN ciclista c 
    ON e.nomequipo = c.nomequipo;

  SELECT * 
    FROM equipo e JOIN ciclista c
    USING (nomequipo);

   SELECT * 
    FROM equipo e JOIN ciclista c 
    WHERE e.nomequipo = c.nomequipo; -- No usar en el JOIN, aunque funciona de igual manera que el ON
  
 /**
  producto cartersiano
 */

  SELECT * 
    FROM ciclista c, equipo e; -- todos con todos, el bueno es el que coincida

 -- convierto producto cartesiano en una combinacion

  SELECT * 
    FROM ciclista c, equipo e
    WHERE e.nomequipo = c.nomequipo; 

/**
  Listarme los nombres del ciclista y del equipo de aquellos ciclisas que hayan ganado puertos
 */

  SELECT DISTINCT c.nombre, c.nomequipo 
    FROM ciclista c JOIN puerto p USING (dorsal);
     
/**
  Listarme los nombres del ciclista y del equipo de aquellos ciclisas que hayan ganado etapas
 */

 SELECT DISTINCT c.nombre, c.nomequipo 
    FROM ciclista c JOIN etapa e USING (dorsal); 

-- c1 (subconsulta)
  SELECT DISTINCT e.dorsal 
    FROM etapa e;

-- completa
SELECT c.nombre, c.nomequipo 
  FROM ciclista c 
    JOIN (SELECT DISTINCT e.dorsal FROM etapa e) c1
    USING (dorsal);

/**
  quiero saber los ciclistas que han ganado puertos y el numero de puertos que han ganado.
  del ciclista quiero saber el dorsal y el nombre.
*/

-- c1 
  SELECT p.dorsal, c.nombre, p.nompuerto 
    FROM puerto p JOIN ciclista c USING (dorsal);

-- completa
  SELECT c1.dorsal, c1.nombre, COUNT(*)numeroPuertos 
    FROM (
      SELECT c.dorsal, c.nombre, p.nompuerto
      FROM puerto p JOIN ciclista c USING (dorsal)
    ) c1
    GROUP BY c1.dorsal, c1.nombre; 

-- sin subconsulta
  SELECT c.dorsal, c.nombre, COUNT(*)numeroPuertos 
    FROM  ciclista c JOIN puerto p USING (dorsal)
    GROUP BY c.dorsal, c.nombre;
    
  
