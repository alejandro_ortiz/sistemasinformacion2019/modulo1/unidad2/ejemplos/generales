﻿USE ciclistas;

/** 
  proyeccion
**/

  -- Sacamos los nombres de los ciclistas.
  -- Usamos el DISTINCT para que si tenemos datos repetidos solo nos los saque una vez.
  -- Si el campo que se quiere sacar en Primary Key o Unique Key no es necesario el DISTINCT.

  SELECT DISTINCT c.nombre FROM ciclista AS c; -- la palabra AS es opcional 

  SELECT DISTINCT c.nombre FROM ciclista c;

  SELECT DISTINCT ciclista.nombre FROM ciclista;

  SELECT DISTINCT nombre FROM ciclista;

  /**
    seleccion
  **/

  -- se pueden escribir en varias lineas o en una sola

  SELECT 
    * 
  FROM 
    ciclista c 
  WHERE 
    c.edad<30;

  SELECT * FROM ciclista c WHERE c.edad<30;

  /**
    seleccion + proyeccion
  **/

  SELECT 
    DISTINCT c.nombre
  FROM 
    ciclista c 
  WHERE 
    c.edad<30;

  -- indicame las edades de los ciclistas de banesto

  SELECT 
    DISTINCT c.edad 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Banesto';

  -- indicame los nombres de los equipos de los ciclistas que tienes menos de 30 años

  SELECT 
    DISTINCT c.nomequipo 
  FROM 
    ciclista c 
  WHERE 
    c.edad<30; 

  /**
    and y or
  **/

  -- listar los nombres de los ciclistas cuya edad esta entre 30 y 32(inclusive)  
  SELECT 
    DISTINCT c.nombre
  FROM 
    ciclista c 
  WHERE 
    c.edad>=30 AND c.edad<=32;  

  -- modificar con los operadores extendidos (BETWEEN p1 AND p2)
  SELECT 
    DISTINCT c.nombre 
  FROM 
    ciclista c 
  WHERE 
    c.edad BETWEEN 30 AND 32;

  -- listar los equipos que tengan ciclistas menores de 31 años y que su nombre comience por M

  SELECT 
    DISTINCT c.nomequipo
  FROM 
    ciclista c 
  WHERE 
    c.edad<31 AND c.nombre LIKE 'M%';

  -- listar los equipos que tengan ciclistas menores de 31 años o que su nombre comience por M

  SELECT 
    DISTINCT c.nomequipo
  FROM 
    ciclista c 
  WHERE 
    c.edad<31 OR c.nombre LIKE 'M%';

  -- listar los ciclistas de banesto y de kelme

  SELECT 
    *
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Banesto' OR c.nomequipo='Kelme';

  -- modificar con los operadores extendidos (IN (p1,p2))

  SELECT 
    * 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo IN('Banesto','Kelme');