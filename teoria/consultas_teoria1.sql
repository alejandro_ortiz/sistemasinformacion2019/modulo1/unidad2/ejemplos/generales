﻿USE teoria1;

-- 1. Cuantos coches a alquilado el usuario 1

  -- numero de alquileres
  SELECT COUNT(*)alquileres 
    FROM alquileres a 
    WHERE a.usuario=1;

  -- numero de coches alquilados 
  SELECT COUNT(DISTINCT a.coche)cochesAlquilados 
    FROM alquileres a 
    WHERE a.usuario=1;

-- 2. Numero de alquileres por mes

  SELECT MONTH(a.fecha)mes ,COUNT(*)alquileres 
    FROM alquileres a 
    GROUP BY MONTH(a.fecha); 

-- 3. numero de usuario por sexo
  
  SELECT u.sexo, COUNT(*)usuarios 
    FROM usuarios u 
    GROUP BY u.sexo;

-- 4. numero de alquileres de coches por color

  SELECT c.color, COUNT(*)numCoches 
    FROM coches c 
    JOIN alquileres a 
    ON c.codigoCoche = a.coche 
    GROUP BY c.color;

-- 5. numero de marcas
  SELECT COUNT(DISTINCT c.marca)marcas 
    FROM coches c;

-- 6. numero de marcas de coches alquilados

  -- alquileres por marca
  SELECT marca, COUNT(*)alquileres 
    FROM coches c 
    JOIN alquileres a ON c.codigoCoche = a.coche 
    GROUP BY c.marca;

  -- numero de marcas
  SELECT COUNT(DISTINCT c.marca)marcasAlquiladas FROM coches c 
    JOIN alquileres a ON c.codigoCoche = a.coche;

-- 7. poblacion con mas hombres

  -- Subconsula C1
  -- Saca los hombres de cada poblacion 
  SELECT u.poblacion, COUNT(*)hombres 
    FROM usuarios u 
    WHERE u.sexo='hombre' 
    GROUP BY u.poblacion;
  
  -- Subconsulta C2
  -- Sacar numero maximo de hombres
  SELECT MAX(C1.hombres)maximo 
    FROM (
      SELECT u.poblacion, COUNT(*)hombres 
        FROM usuarios u 
        WHERE u.sexo='hombre' 
        GROUP BY u.poblacion
    )C1; 

  -- Consulta final
  SELECT C1.poblacion 
    FROM (
      SELECT u.poblacion, COUNT(*)hombres 
        FROM usuarios u 
        WHERE u.sexo='hombre' 
        GROUP BY u.poblacion
    ) C1 
    JOIN (
      SELECT MAX(C1.hombres)maximo 
      FROM (
        SELECT u.poblacion, COUNT(*)hombres 
          FROM usuarios u 
          WHERE u.sexo='hombre' 
          GROUP BY u.poblacion
      )C1
    )C2 
    ON C1.hombres = C2.maximo;