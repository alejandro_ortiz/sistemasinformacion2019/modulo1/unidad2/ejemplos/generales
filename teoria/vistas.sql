﻿USE teoria1;

-- consutas con vistas

-- 1. Indicar el nombre de las marcas que se hayan alquilado coches

  -- Sin Optimizar
  -- crear la vista
  CREATE OR REPLACE VIEW consulta1 AS
    SELECT DISTINCT c.marca 
      FROM alquileres a 
      JOIN coches c 
      ON a.coche = c.codigoCoche;

  -- ejecutar la vista
  SELECT * FROM consulta1 c;

  -- Optimizada
  -- Coches alquilados
  CREATE OR REPLACE VIEW c1Consulta1 AS 
    SELECT DISTINCT a.coche 
          FROM alquileres a;

  -- Consulta final
  CREATE OR REPLACE VIEW consulta1 AS
    SELECT DISTINCT c.marca 
      FROM c1Consulta1 c1
      JOIN coches c 
      ON c1.coche = c.codigoCoche;

  -- ejecutar la vista
  SELECT * FROM consulta1 c;

-- 2. Indicar los nombres de los usuarios que hayan alquilado algun coche
  
  -- Usuarios han alquilado
  CREATE OR REPLACE VIEW c1Consulta2 AS
    SELECT DISTINCT a.usuario 
    FROM alquileres a;

  -- Consulta final
  CREATE OR REPLACE VIEW consulta2 AS
    SELECT DISTINCT u.nombre 
    FROM c1Consulta2 c
    JOIN usuarios u 
    ON c.usuario = u.codigoUsuario;

  -- ejecutar vista
  SELECT * FROM consulta2 c;
             